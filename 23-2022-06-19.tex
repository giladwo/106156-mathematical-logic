\documentclass[12pt, a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts}

\title{Lecture 23}
\author{ Gilad Woloch }
\date{\today}

\newcommand{\proof}{\textbf{Proof}: }
\newcommand{\definition}{\textbf{Definition}: }
\newcommand{\theorem}{\textbf{Theorem}: }
\newcommand{\conclusion}{\textbf{Conclusion}: }
\newcommand{\claim}{\textbf{Claim}: }
\newcommand{\example}[2]{\textbf{Example {#1}}: {#2}\\}
\newcommand{\pair}[2]{\left( {#1}, {#2} \right)}

\begin{document}

\maketitle

\section{Untitled}

\definition
Let \(\Sigma\) be a theory and let \(\kappa\) be its cardinal.
We say that \(\Sigma\) is \underline{\(\kappa\)-categorical} if there exists a
model \(M\) of \(\Sigma\) with \(|W^M| = \kappa\), and any two such models are
isomorphic.

\example{1}{Group theory}
Let \(\Sigma\) be group theory.
First, check its category for finite \(\kappa\).
\begin{enumerate}
    \item \(\kappa = 1\): yes
    \item \(\kappa = 2\): yes
    \item \(\kappa = 3\): yes
    \item \(\kappa = 4\): no
\end{enumerate}
More generally, group theory is \(p\)-categorical for all primes \(p\), and
there are additional finite \(\kappa\) such that it is \(\kappa\)-categorical.

For all infinite cardinals \(\kappa\), group theory is not
\(\kappa\)-categorical.
(For instance, because there exist both an Abelian group and a non-Abelian
group of the same cardinal).


\example{2}{Dense linear order theory without a minimum or a maximum}
Inspect a language that has a binary relation symbol \(R\) and the the binary
relation symbol \(=\).

Let \(\Sigma\) be the theory that contains the following propositions:
\begin{description}
    \item[reflexivity] \((\forall x) R(x, x)\)
    \item[transitivity] \((\forall x)(\forall y)(\forall z) ((R(x, y) \land R(y, z)) \rightarrow R(x, y))\)
    \item[anti-symmetry] \((\forall x)(\forall y)((R(x, y) \land R(y, x)) \rightarrow x = y)\)
    \item[linear order]
        \begin{eqnarray*}
            (\forall x)(\forall y)(R(x, y) \lor R(y, x)) \\
            (\forall x)(\forall y)((R(x, y) \land \neg x = y) \rightarrow
                (\exists z)(R(x, z) \land R(z, y) \land \neg x = z \land \neg z =y))
        \end{eqnarray*}
    \item[no minimum] \(\neg (\exists x)(\forall y) R(x, y)\)
    \item[no maximum] \(\neg (\exists x)(\forall y) R(y, x)\)
\end{description}

All models of \(\Sigma\) have infinite worlds, hence the categorical
distinction arises only for infinite cardinals.

First, assume \(\kappa = \aleph_0\).
Then the set of rational numbers \(\mathbb{Q}\) with the normal order is a
model of \(\Sigma\) with a countable world.

There exists a theorem of Cantor that states that all countable sets with dense
linear order without a minimum and without a maximum is isomorphic to the
rationals with the normal order.

This means that our \(\Sigma\) is \(\aleph_0\)-categorical.

Now, assume \(\kappa = 2^{\aleph_0}\) (the continuum's cardinal).
Then the set of real numbers \(\mathbb{R}\) with the normal order is a model of
\(\Sigma\) with a world of cardinality \(2^{\aleph_0}\).

The set of points on the plane \(\mathbb{R}^2\) with the lexicographical order
is a model of \(\Sigma\) with a world of cardinality \(2^{\aleph_0}\) as well.

Notice that the above two models are not isomorphic:

For all bounded non-empty subsets of the real numbers
\(A \subseteq \mathbb{R}\), \(A\) has a a supremum.

An isomorphism between these models would preserve this property.

However, in the second this property is not satisfied.

Thus, the two models are not isomorphic, so the theory \(\Sigma\) is not
\(@^{\aleph_0}\)-categorical.

Furthermore, one can show that for all cardinals \(\kappa > \aleph_0\), the
theory \(\Sigma\) is not \(\kappa\)-categorical.

\example{3}{Non-circular bijective function theory}

Inspect a language that contains an unary function symbol \(f\) and the binary
relation symbol \(=\).

Let \(\Sigma\) be the following set of propositions:
\begin{description}
    \item[injective] \((\forall x)(\forall y)(f(x) = f(y) \rightarrow x = y)\)
    \item[surjective] \((\forall y)(\exists x) f(x) = y\)
    \item[no loops] \((\forall x) \neg f(x) = x\)
    \item[no 2-cycles] \((\forall x) \neg f(f(x)) = x\)
    \item[no 3-cycles] \((\forall x) \neg f(f(f(x))) = x\)

        and so on
\end{description}

First, analyze the structure of the models of this theory \(\Sigma\).

Take some element \(a\) of the world.
\[
    \underbrace{\dots \mapsto a^{--} \mapsto a^- \mapsto}_{\text{\(f\) is surjective}} \underbrace{a}_{\text{origin}} \underbrace{\mapsto a^+ \mapsto a^{++} \mapsto \dots}_{\text{no loops or cycles}}
\]

In this way we get an infinite chain in both directions where the function
sends each element to its immediate right neighbor.

The chain may be the whole world.

Otherwise, take some element \(b\) of the world that is not in the chain and
repeat te process
\[
    \underbrace{\dots \mapsto b^{--} \mapsto b^- \mapsto}_{\text{\(f\) is surjective}} \underbrace{b}_{\text{origin}} \underbrace{\mapsto b^+ \mapsto b^{++} \mapsto \dots}_{\text{no loops or cycles}}
\]

In this way we get another chain that is disjoint with the previous chain.

These two chains may be the whole world.

Otherwise, we can repeat the process.

This shows that for all models of \(\Sigma\) the world is a disjoint union
(finite or infinite) of such chains, and all such disjoint unions are models of
\(\Sigma\).

Since all models of \(\Sigma\) have infinite worlds, the categorical
distinction arises only for infinite cardinalities.

First, assume \(\kappa = \aleph_0\).

Inspect a model \(M\) that is formed by a single chain and a model \(N\) that
is formed by two such chains.

\begin{center}
    \begin{tabular}{c|c}
        & \(\dots \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \dots\) \\
        \( \dots \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \dots \) & \\
        & \(\dots \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \dots\) \\
        \( M \) & \( N \) \\
    \end{tabular}
\end{center}

These two models are not isomorphic, despite the fact that both have worlds of
cardinality \(\aleph_0\).

If an isomorphism from \(M\) to \(N\) exists, it has to send \(M\) to one of
\(N\)'s chains, hence it is not surjective (on \(W^N\)).

Thus, \(\Sigma\) is not \(\aleph_0\)-categrical.

Now, assume \(\kappa > \aleph_0\).

Let \(M\) be a model of \(\Sigma\) with a world that is of cardinality
\(\kappa\).

Assume it is formed by \(\lambda\) such chains.

Then \(\lambda\) is an infinite cardinal and the following holds
\[
    \kappa = \lambda \cdot \aleph_0 = \lambda
\]

Which means that all models \(M\) of \(\Sigma\) with a world of cardinality
\(\kappa\) is formed by \(\kappa\) such chains, hence any two such models are
isomorphic.

Therefore, our \(\Sigma\) is \(\kappa\)-categorical for all cardinals
\(\kappa > \aleph_0\).


\definition
Let \(\Sigma\) be a theory in language \(L\).
\(\Sigma\) is called \underline{complete} if for all propositions \(\alpha\)
in language \(L\), either \(\Sigma \vdash \alpha\) or
\(\Sigma \vdash \neg \alpha\).

\claim
Let \(\Sigma\) be a theory in language \(L\)>
Then \(\Sigma\) is complete if and only if all models of \(\Sigma\) satisfy
the same propositions, meaning:

For all two models \(M_1\), \(M_2\) of \(\Sigma\) and for all propositions
\(\alpha\) in language \(L\),
\[
    M_1 \models \alpha \Longleftrightarrow M_2 \models \alpha 
\]

\begin{proof}
    \underline{First direction}

    Assume \(\Sigma\) is complete.

    Let \(M_1\), \(M_2\) be two models of \(\Sigma\) and let \(\alpha\) be a
    proposition in language \(L\).

    From the definition, there are two cases:

    \begin{enumerate}
        \item \(\Sigma \vdash \alpha\)

            Then, from the strong soundness theorem, \(\alpha\) is satisfied in
            all models of \(\Sigma\), and specifically
            \[
                M_1 \models \alpha
            \]
            \[
                M_2 \models \alpha
            \]

        \item \(\Sigma \vdash \neg \alpha\)

            Then, from the strong soundess theorem, \(\neg \alpha\) is
            satisfied in all models of \(\Sigma\), and specifically
            \[
                M_1 \models \neg \alpha
            \]
            \[
                M_2 \models \neg \alpha
            \]
            which means
            \[
                M_1 \not \models \alpha
            \]
            \[
                M_2 \not \models \alpha
            \]
    \end{enumerate}


    \underline{Second direction}

    Assume any two models of \(\Sigma\) satisfy the same propositions.

    Let \(\alpha\) be a proposition in language \(L\).

    Then there are two cases:

    \begin{enumerate}
        \item \(\alpha\) is satisfied in all models of \(\Sigma\)

            Then from the strong soundness theorem, \(\Sigma \vdash \alpha\).


        \item \(\alpha\) is not satisfied in any model of \(\Sigma\)

            \(\alpha\) is a proposition, and all structures satisfy either a
            proposition or its negation, thus \(\neg \alpha\) is satisfied in
            all models of \(\Sigma\).

            From the strong completeness theorem, \(\Sigma \vdash \neg \alpha\).
    \end{enumerate}
\end{proof}


\example{1}{Group theory}

Group theory is not complete, because there are propositions that are satisfied
in some groups and not in others, for instance the proposition that states
"the group is Abelian":
\[
    (\forall x)(\forall y) f(x, y) = f(y, x)
\]


\end{document}
