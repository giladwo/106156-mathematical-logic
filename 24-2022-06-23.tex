\documentclass[12pt, a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts}

\title{Lecture 24}
\author{ Gilad Woloch }
\date{\today}

\newcommand{\proof}{\textbf{Proof}: }
\newcommand{\definition}{\textbf{Definition}: }
\newcommand{\theorem}{\textbf{Theorem}: }
\newcommand{\conclusion}{\textbf{Conclusion}: }
\newcommand{\claim}{\textbf{Claim}: }
\newcommand{\example}[2]{\textbf{Example {#1}}: {#2}\\}
\newcommand{\pair}[2]{\left( {#1}, {#2} \right)}

\begin{document}

\maketitle

\section{Untitled}

\theorem
Let \(\Sigma\) be a theory in language \(L\) and assume
\begin{enumerate}
    \item \(\Sigma\) does not have a model with a finite world.\
    \item There exists a cardinal \(\kappa \ge |L|\) such that \(\Sigma\) is
        \(\kappa\)-categorical
\end{enumerate}
Then \(\Sigma\) is complete.

\begin{proof}
    Assume towards contradiction that \(\Sigma\) is not complete.

    From the previous claim, there exists a sentence (proposition) \(\alpha\)
    in language \(L\) and there exist models \(M_1\), \(M_2\) of \(\Sigma\)
    such that one of them satisfies \(\alpha\) and the other does not.

    WLOG
    \[
        M_1 \models \alpha
    \]
    \[
        M_2 \not \models \alpha
    \]

    Since \(\alpha\) is a sentence,
    \[
        M_2 \models \neg \alpha
    \]

    Now inspect the following theories:
    \[
        \Sigma_1 = \Sigma \cup \left\{ \alpha \right\}
    \]
    \[
        \Sigma_2 = \Sigma \cup \left\{ \neg \alpha \right\}
    \]

    Then \(M_1\) is a model of \(\Sigma_1\) and \(M_2\) is a model of
    \(\Sigma_2\).

    The cardinal \(\kappa\) that appears in the theorem is infinite (from the
    first part) and satisifes \(\kappa \ge |L|\).

    Thus L\"owenheim-Skolem theorem can be used separately on each of the
    theories \(\Sigma_1\), \(\Sigma_2\).

    We get a model \(N_1\) of \(\Sigma_1\) with \(|W^{N_1}| = \kappa\) and
           a model \(N_2\) of \(\Sigma_2\) with \(|W^{N_2}| = \kappa\).

    Now, both models of \(\Sigma\) are with a world of cardinality \(\kappa\)
    and \(\Sigma\) is \(\kappa\)-categorical, \(N_1\), \(N_2\) are isomorphic.

    Thus, they satisfy the same sentences.

    But
    \[
        N_1 \models \alpha
    \]
    \[
        N_2 \models \neg \alpha
    \]
    which is a contradiction.

\end{proof}


To demonstrate a use case of the theorem, recall two previous theory examples
we've already their categories.

\example{2}{Dense linear order without a minimum and without a maximum theory}

We've seen that this theory \(\Sigma\) does not have a model with a finite
world and that is is \(\aleph_0\)-categorical.

Applying the theorem for \(\kappa = \aleph_0\) means that \(\Sigma\) is
complete.

On the other hand, we've also seen that \(\Sigma\) has models with worlds of
cardinality \(2^{\aleph_0}\) which are not isomorphic:

\begin{description}
    \item[\(M\):] The reals \(\mathbb{R}\) with the normal order
    \item[\(N\):] The plane \(\mathbb{R}^2\) with the lexicographic order
\end{description}

We've shown these are not isomorphic because in \(M\) all non-empty bounded
subsets have supermums, and in \(N\) this property doesn't hold.

Nevertheless, from the completeness of \(\Sigma\) we conclude that \(M\), \(N\)
satisfy the same sentences.

This is not a contradiction, because the above property used for the
distinction cannot be expressed by a sentence in the language (it is not a
first-order proposition).

\example{3}{Injective, surgective and cycleless function theory}

We've seen this theory \(\Sigma\) has no models with a finite world and is
\(\kappa\)-categorical for all cardinals \(\kappa > \aleph_0\).

Applying the theorem for such \(\kappa\) means that \(\Sigma\) is complete.

On the other hand, we've also seen that \(\Sigma\) has models of countable
cardinalities that are not isomorphic:

\begin{center}
    \begin{tabular}{c|c}
        & \(\dots \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \dots\) \\
        \( \dots \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \dots \) & \\
        & \(\dots \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \bullet \mapsto \dots\) \\
        \( M \) & \( N \) \\
    \end{tabular}
\end{center}

A convenient way to distinguish them:

\(M\) is connected, meaning for all two elements, one can be reached from the
other by repeated applications of the function.

\(N\) is not connected in this sense.

Nevertheless, from the completeness of \(\Sigma\), \(M\) and \(N\) satisfy the
same sentences.

This is not a contradiction, because the connectivity property cannot be
expressed as a sentence in the language (it is not a first-order proposition).

This relates to the aforementioned fact that connected graph theory is not
a first-order theory.


\definition

Let \(\Sigma\) be a theory in language \(L\).

We say that \(\Sigma\) is \underline{recursive} if there exists an algorithm
that decides whether a sentence is contained in \(\Sigma\).

In other words, there exists an algorithm that, given some sentence \(\alpha\)
in the language as an input, performs a finite number of operations and on
conclusion outputs correctly whether \(\alpha \in \Sigma\) or not.

\underline{Note}: All finite theories are recursive.

An infinite theory that is described explicitly is recursive as well.

\end{document}
