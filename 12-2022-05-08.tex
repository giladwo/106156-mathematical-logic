\documentclass[12pt, a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts}

\title{Lecture 12}
\author{ Gilad Woloch }
\date{\today}

\newcommand{\proof}{\textbf{Proof}: }
\newcommand{\definition}{\textbf{Definition}: }
\newcommand{\theorem}{\textbf{Theorem}: }
\newcommand{\conclusion}{\textbf{Conclusion}: }
\newcommand{\claim}{\textbf{Claim}: }
\newcommand{\example}[1]{\textbf{Example {#1}}: }
\newcommand{\pair}[2]{\left( {#1}, {#2} \right)}

\begin{document}

\maketitle

\section{Truthness definition}

Given a formula \(\alpha\) in lanugage \(L\), structure \(M\) for \(L\)
and substitution \(s\) for \(M\), we want to define
\[
    M \models_{s} \alpha
\]
which means: formula \(\alpha\) is \underline{true in structure \(M\) and
substitution \(s\)}, or in other words: the structure \(M\) and substitution
\(s\) \underline{satisfy} the formula \(\alpha\).
We also write
\[
    M \not\models_{s} \alpha
\]
if \(M \models_{s} \alpha\) is not true, meaning formula \(\alpha\) is false
in structure \(M\) and substitution \(s\).

The definition is inductive on the structure of the formula \(\alpha\).

If \(\alpha\) is an atomic formula of the form
\[
    R \left( t_1, \dots, t_k \right)
\]
where \(R\) is a \(k\)-ary relation symbol, then
\[
    M \models_{s} \alpha \Leftrightarrow
    \left( s^* \left( t_1 \right), \dots, \left( t_k \right) \right) \in R^M
\]

If \(\alpha\) is a compound formula that is built from one or more simpler
formulae, then:

\begin{enumerate}
    \item By a connector
        \begin{enumerate}
            \item If \(\alpha\) is of the form \(\neg \beta\) then
                \[
                    M \models_{s} \alpha \Leftrightarrow
                    M \not \models_{s} \beta
                \]

            \item If \(\alpha\) is of the form \(\beta \land \gamma\) then
                \[
                    M \models_{s} \alpha \Leftrightarrow
                    M \models_{s} \beta \text{and also} M \models_{s} \gamma
                \]
        \end{enumerate}
        and so on for the rest of the connectors, like in proposition calculus.

    \item By a quantifier
        \begin{enumerate}
            \item If \(\alpha\) is of the form
                \(\left( \forall x \right) \beta\) then
                \[
                    M \models_{s} \alpha \Leftrightarrow M \models_{s'} \beta
                \]
                for all substitutions \(s'\) that coalesces with \(s\), except
                maybe for \(x\), it holds.

            \item If \(\alpha\) is of the form
                \(\left( \exists x \right) \beta\) then
                \[
                    M \models_{s} \alpha \Leftrightarrow M \models_{s'} \beta
                \]
                for a substitution \(s'\) that coalesces with \(s\),
                except maybe for \(x\).
        \end{enumerate}
\end{enumerate}


\example{}
Let \(L\) be a language that contains a binary relation symbol \(R\).
Let \(M\) be a structure for \(L\) where
\[
    W^M = \left\{ 0, 1, 2, \dots \right\}
\]
\[
    R^M = \left\{ \left( a, b \right) \in \left( W^M \right)^2 \colon a < b \right\}
\]
Now inspect a few formulae in language \(L\):
\begin{enumerate}
    \item Let \(\alpha\) be the atomic formula \(R \left( x, y \right)\).
        Then for instance, if \(s\) is a substitution for \(M\) where
        \(s \left( x \right) = 3\), \(s \left( y \right) = 8\) then
        \[
            M \models_{s} R \left( x, y \right)
        \]
        But if \(s\) is a substitution for \(M\) where
        \(s(x) = 5\), \(s(y) = 2\) then
        \[
            M \not \models_{s} R \left( x, y \right)
        \]

    \item Let \(\alpha\) be the formula
        \(\left( \exists x \right) R \left( x, y \right)\).
        \par let \(s\) be a substition for \(M\) where \(s(x) = 4\), \(s(y) = 3\).
        \par then
        \[
            M \not \models_{s} R \left( x, y \right)
        \]
        but if \(s\) is changed to \(s'\) that is different from \(s\) only by
        satisfying \(s' \left( x \right) = 1\) then
        \[
            M \models_{s'} R \left( x, y \right)
        \]
        Therefore, according to the definition of truthness,
        \[
            M \models_{s} \left( \exists x \right) R \left( x, y \right)
        \]
        \par More generally,
        \[
            M \models_{s} \left( \exists x \right) R \left( xx, y \right)
            \Leftrightarrow s \left( y \right) > 0
        \]

    \item Let \(\alpha\) be the formula
        \( \left( y \right) \left( \exists x \right) R \left( x, y \right)\).
        \par Let \(s\) be a substitution for \(M\) where \(s(x) = 5\), \(s(y) = 3\).
        Then by changing the result it yields on \(x\) we can make it satisfy
        \(R \left( x, y \right)\), thus
        \[
            M \models_{s} \left( \exists x \right) R \left( x, y \right)
        \]
        But if we change the result it yields on \(y\) such that \(s'(y) = 0\)
        then
        \[
            M \not \models_{s'} \left( \exists x \right) R \left( x, y \right)
        \]

        Therefore, according to the definition of truthness,
        \[
            M \not \models_{s} \left( \forall y \right) \left( \exists x \right) R \left( x, y \right)
        \].
        Moreover, for all substitions \(s\)
        \[
            M \not \models_{s} \left( \forall y \right) \left( \exists x \right) R \left( x, y \right)
        \]
\end{enumerate}

Given a formula \(\alpha\) in language \(L\) and a structure \(M\) for \(L\),
we say that \(\alpha\) is \underline{true} in \(M\), and write
\[
    M \models \alpha
\]
if for all substitutions \(s\) for \(M\) it holds that
\[
    M \models_{s} \alpha
\]

For instance, in the structure from the previous example
\[
    M \models R \left( x, y \right) \rightarrow \neg R \left( y, x \right)
\]

Given a formula \(\alpha\) in langugage \(L\), we say that \(\alpha\) is
\underline{logically true} and write
\[
    \models \alpha
\]
if for all structures \(M\) for \(L\) and for all substitutions \(s\) for \(M\)
it holds that
\[
    M \models_{s}
\]

Given a formula \(\alpha\), an instance of individual constants \(x\) in it
is called \underline{bound} if it is in scope of a quantifier of the form
\( \left( \forall x \right) \) or \( \left( \exists x \right) \).
Otherwise, the apperance is called \underline{free}.

For instance, in the formula
\[
    \underbrace{\left( \exists
        \underbrace{x}_{\text{bound}} \right) R \left(
        \underbrace{x}_{\text{bound}},
        \underbrace{y}_{\text{free}} \right)
    }_{\text{scope of \(\left( \exists x \right) \)}}
    \rightarrow
    \underbrace{\left( \exists 
        \underbrace{z}_{\text{bound}} \right) P \left( 
        \underbrace{x}_{\text{free}}, 
        \underbrace{z}_{\text{bound}} \right)
    }_{\text{scope of \(\left( \exists z \right)\)}}
\]
on the right hand side:
z is bound
x is free

The two first instances of \(x\) are bound but the third is free, the
instance if \(y\) is free, and the two instances of \(z\) are bound.

Given a formula \(\alpha\), we say that a variable \(x\) \underline{appears
freely} in formula \(\alpha\) if there exists a free instance of \(x\) in
\(\alpha\).

Let us denote the set of variables that freely appear in \(\alpha\) as
\(FV \left( \alpha \right)\).

For instance, in the last example
\[
    FV \left( \alpha \right) = \left\{ x, y \right\}
\]

A formula \(\alpha\) is called a \underline{proposition} if
\(FV \left( \alpha \right) = \emptyset\).

For isntance, the following formula in the lanugage of group theory
\[
    \left( \forall x \right)
        \left( \forall y \right) f \left( x, y \right) = f \left( y, x \right)
\]
is a propotision that means the group is abelian.

\claim Let \(\alpha\) be a formula in language \(L\), let \(M\) be a structure
for \(L\) and let \(s_1\), \(s_2\) be two substitutions for \(M\).

Assume \(s_1\), \(s_2\) coalesce on \(FV \left( \alpha \right) \).
Then
\[
    M \models_{s_1} \alpha \Leftrightarrow M \models_{s_2} \alpha
\]

\conclusion The truthness of a proposition in a gven structure is independent
of substitution.

\underline{Note}: The truthness of a formula of the form
\[
    \left( \forall x \right) \left( \exists y \right) \beta
\]
in substitution \(s\), according to the definition of truthness, requires
that for all substitions \(s'\) that coalesces with \(s\), except maybe for
\(x\), \(\left( \exists x \right) \beta\) is true.
But in \(\left( \exists x \right) \beta\), \(x\) is not a free variable, thus
its truthness under \(s'\) is like under \(s\).

Meaning
\[
    M \models_{s} \left( \forall x \right) \left( \exists x \right) \beta
    \Leftrightarrow M \models_{s} \left( \exists x \right) \beta
\]

To prove the claim, first phrase and prove a helper claim that is parallel but
is about nouns instead of formulae.

\underline{Helper claim}:
Let \(t\) be a noun in language \(L\), let \(M\) be a structure for \(L\) and
let \(s_1\), \(s_2\) be two substitutions for \(M\).
Assume that \(s_1\), \(s_2\) coalesce on all individual constants that appear
in \(t\).
Then
\[
    s_1^* \left( t \right) = s_2^* \left( t \right)
\]

\emph{midterm test on 2.6.22}

\end{document}
