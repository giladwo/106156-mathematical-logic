\documentclass[12pt, a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts}

\title{Lecture 26}
\author{ Gilad Woloch }
\date{\today}

\newcommand{\proof}{\textbf{Proof}: }
\newcommand{\definition}{\textbf{Definition}: }
\newcommand{\theorem}{\textbf{Theorem}: }
\newcommand{\conclusion}{\textbf{Conclusion}: }
\newcommand{\claim}{\textbf{Claim}: }
\newcommand{\lemma}{\textbf{Lemma}: }
\newcommand{\example}[2]{\textbf{Example {#1}}: {#2}\\}
\newcommand{\pair}[2]{\left( {#1}, {#2} \right)}

\begin{document}

\maketitle

\section{G\"odel's Incompleteness Theorem}

Let \(\Sigma\) be a theory in the language of number theory.

Assume:
\begin{itemize}
    \item \(N\) is a model of \(\Sigma\)
    \item \(\Sigma\) is recursive
\end{itemize}

Then \(\Sigma\) is incomplete.


\lemma
Let \(\Sigma\) be a theory as in the theorem.

Then there exists a formula \(\alpha\) in the language of number theory that
has three free variables \(x_1\), \(x_2\), \(x_3\) that has the following
property:

For a 3-tuple \(k\), \(l\), \(m\) of natural numbers
\[
    \alpha \left( s^k(c), s^l(c), s^m(c) \right)
\]
is the sentence that is given by applying to \(\alpha\) a free substitution of

\(S^k(c)\) replacing \(x_1\)

\(S^l(c)\) replacing \(x_2\)

\(S^m(c)\) replacing \(x_3\)

Then for all natural \(k\), \(l\), \(m\):

\[
    N \models \alpha \left( S^k(c), S^l(c), S^m(c) \right)
\]

\[
    \Updownarrow
\]

\(k\) is the G\"odel number of formula \(b\) with a single free variable.
\(m\) is the G\"odel number of a proof from \(\Sigma\) of the sentence that is
given from \(b\) by application of a free substitution of \(S^l(c)\) replacing
the free variable.



\subsection{Proof of the theorem based on the lemma}

\begin{proof}
    Let \(\alpha\) be a formula that satisfies the lemma.

    Inspect the following formula \(\beta\):
    \[
        (\forall x_3) \neg \alpha ( x_1, x_1, x_3 )
    \]

    This is a formula with a single free variable \(x_1\).

    Let \(k\) be the G\"odel number of formula \(\beta\).

    Next, inspect the following sentence \(\phi\):
    \[
        (\forall x_3) \neg \alpha ( S^k(c), S^k(c), x_3 )
    \]

    This is the sentence given by applying to formula \(\beta\) a free
    substitution of \(S^k(c)\) replacing \(x_1\).

    The next claim shows that sentence \(\phi\) states itself is not provable
    from \(\Sigma\).

    \claim
    \[
        N \models \phi \Leftrightarrow \Sigma \not \vdash \phi
    \]

    \underline{Claim proof}:

    Let \(k\) be the G\"odel number of formula \(\beta\).

    Notice that for all natural \(m\), according to the lemma, it holds that
    \[
        N \models \alpha \left( S^k(c), S^k(c), S^m(c) \right)
    \]
    \[
        \Updownarrow
    \]
    \(m\) is the G\"odel number of a proof from \(\Sigma\) of \(\phi\).



    Next,
    \[
        N \models \phi
    \]
    \[
        \Updownarrow
    \]
    \[
        N \models (\forall x_3) \neg \alpha \left( S^k(c), S^k(c), x_3 \right)
    \]
    \[
        \Updownarrow
    \]
    For all naturals \(m\),
    \[
        N \models \neg \alpha \left( S^k(c), S^k(c), S^m(c) \right)
    \]
    \[
        \Updownarrow
    \]
    For all naturals \(m\), \(m\) is not the G\"odel number of a proof from
    \(\Sigma\) of \(\phi\)
    \[
        \Updownarrow
    \]
    \[
        \Sigma \not \vdash \phi
    \]
    As required.

    Next, prove the two following facts:
    \begin{enumerate}
        \item \(\Sigma \not \vdash \phi\)
        \item \(\Sigma \not \vdash \neg \phi\)
    \end{enumerate}

    \underline{Proof of 1}:
    Falsely assume that \(\Sigma \vdash \phi\).

    From the strong soundness theorem, \(\phi\) is satisfied in all models of
    \(\Sigma\) and specifically \(N \models \phi\).

    But from the claim, this means that \(\Sigma \not \vdash \phi\), which is a
    contradiction.


    \underline{Proof of 2}:
    Falsely assume that \(\Sigma \vdash \not \phi\).

    From the strong soundness theorem, \(\neg \phi\) is satisfied in all models
    of \(|Sigma\), and specifically \(N \models \neg \phi\).

    But from part 1, \(\Sigma \not \vdash \phi\), hence from the claim
    \(N \models \phi\), which is a contradiction.

    Parts 1 and 2 show that \(\Sigma\) is incomplete, as required.
\end{proof}


\section{G\"odel's Second Incompleteness Theorem}

Using the arithmeticization of the syntactics one can formulate a sentence that
says "\(\Sigma\) is contradiction-free".

Denote this sentence as \(Cons(\Sigma)\) (read: consistent).

One can wonder if
\[
    \Sigma \vdash Cons(\Sigma)
\]
holds for any theory \(\Sigma\) (that is strong enough to formulate such a
sentence)?

There are two possibilities:
\begin{enumerate}
    \item \(\Sigma\) is contradictory.

        In this case
        \[
            \Sigma \vdash Cons(\Sigma)
        \]

    \item \(\Sigma\) is contradiction-free.

        In this case, G\"odel's Second Incompleteness Theorem states that
        \[
            \Sigma \not \vdash Cons(\Sigma)
        \]
\end{enumerate}

\end{document}
