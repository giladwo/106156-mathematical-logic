\documentclass[12pt, a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts}

\title{Lecture 13}
\author{ Gilad Woloch }
\date{\today}

\newcommand{\proof}{\textbf{Proof}: }
\newcommand{\definition}{\textbf{Definition}: }
\newcommand{\theorem}{\textbf{Theorem}: }
\newcommand{\conclusion}{\textbf{Conclusion}: }
\newcommand{\claim}{\textbf{Claim}: }
\newcommand{\example}[1]{\textbf{Example {#1}}: }

\begin{document}

\maketitle

\section{Untitled}

\underline{Helper claim}:
Let \(t\) be term in language \(L\), let \(M\) be a structure for \(L\)
and let \(s_1\), \(s_2\) be two substitutions for \(M\).
Assume that \(s_1\), \(s_2\) coalesce on all individual constants that appear
in \(t\).
Then
\[
    s_1^*(t) = s_2^*(t)
\]

\proof By induction on the structure of \(t\).

\begin{itemize}
    \item If \(t\) is an atomic term then
        \begin{itemize}
            \item if \(t\) is an individual variable \(x\) then
                \[
                    s_1^*(t) = s_1(x) = s_2(x) = s_2^*(x)
                \]
            \item if \(t\) is an individual constant \(c\) then
                \[
                    s_1^*(t) = c^M = s_2^*
                \]
        \end{itemize}
    \item If \(t\) is of the form
        \[
            f(t_1, \dots, t_k)
        \]
        where \(f\) is a \(k\)-ary function symbol and
        \(t_1, \dots, t_k\) are simpler terms,
        then since for all \(1 \le i \le k\), all individual variables
        that appear in \(t_i\) also appear in \(t\), \(s_1\), \(s_2\) coalesce
        on all individual variables that appear in \(t_i\), thus from the
        induction hypothesis \(s_1^*(t_i) = s_2^*(t_i)\).
        Therefore
        \begin{align*}
            s_1^*(t) &= f^M(s_1^*(t_1), \dots, s_1^*(t_k)) \\
                     &= f^M(s_2^*(t_1), \dots, s_2^*(t_k)) \\
                     &= s_2^*(t)
        \end{align*}
\end{itemize}


\claim Let \(\alpha\) be a formula in language \(L\), let \(M\) be a structure
for \(L\) and let \(s_1\), \(s_2\) be two substitutions for \(M\).
Assume that \(s_1\), \(s_2\) coalesce on \(FV(\alpha)\).
Then
\[
    M \models_{s_1} \alpha \Longleftrightarrow M \models_{s_2} \alpha
\]

\proof By induction on the structure of formula \(\alpha\).

\begin{itemize}
    \item If \(\alpha\) is an atomic formula of the form
        \[
            R(t_1, \dots, t_k)
        \]
        where \(R\) is a \(k\)-ary relation symbol,
        then since for all \(1 \le i \le k\), all individual variables
        that appear in \(t_i\) also appear in \(\alpha\),
        \(s_1\), \(s_2\) coalesce on all individual variables that appear in
        \(t_i\), thus according to the helper claim
        \[
            s_1^*(t_i) = s_2^*(t_i)
        \]
        therefore
        \begin{align*}
            M \models_{s_1} \alpha
            & \Longleftrightarrow \left( s_1^*(t_1), \dots, s_1^*(t_k) \right) \in R^M \\
            & \Longleftrightarrow \left( s_2^*(t_1), \dots, s_2^*(t_k) \right) \in R^M \\
            & \Longleftrightarrow M \models_{s_2} \alpha \\
        \end{align*}

    \item If \(\alpha\) is a compound formula that is built from one or more
        simpler formulae, then one of the following cases apply:
        \begin{itemize}
            \item By a connector
                \begin{itemize}
                    \item \(\alpha\) is of the form \(\neg \beta\).
                        In this case \(FV(\beta) = FV(\alpha)\) so we can use
                        the induction hypothesis on \(\beta\) and get
                        \[
                            M \models_{s_1} \alpha
                            \Longleftrightarrow M \not \models_{s_1} \beta
                            \Longleftrightarrow M \not \models_{s_2} \beta
                            \Longleftrightarrow M \models_{s_2} \alpha
                        \]

                    \item \(\alpha\) is of the form \(\beta \land \gamma\).
                        In this case \(FV(\alpha) = FV(\beta) \cup FV(\gamma)\)
                        so \(s_1\), \(s_2\) in particular coalesce on
                        \(FV(\beta)\), \(FV(\gamma)\) and by using the induction
                        hypothesis on \(\beta\), \(\gamma\) we get
                        \begin{align*}
                            M \models_{s_1} \alpha
                            & \Longleftrightarrow M \models_{s_1} \beta \text{and also} M \models_{s_1} \gamma \\
                            & \Longleftrightarrow M \models_{s_2} \beta \text{and also} M \models_{s_2} \gamma \\
                            & \Longleftrightarrow M \models_{s_2} \alpha \\
                        \end{align*}
                        Continue in a similar way for the rest of the connectors.
                \end{itemize}
            \item By a quantifier
                \begin{itemize}
                    \item \(\alpha\) is of the form \((\forall x) \beta\).
                        In this case \(FV(\alpha) = FV(\beta) \setminus \left\{ x \right\}\).
                        For all elements \(a \in W^M\), denote for \(i = 1, 2\)
                        \(s_i^a\) the substitution we get from \(s_i\) when
                        we set \(s_i^a(x) = a\).
                        Then for all given \(a\) the two subsitutions \(s_1^a\), \(s_2^a\)
                        coalesce on \(FV(\beta)\).
                        Therefore, from the induction hypothesis, for all given \(a\)
                        \[
                            M \models_{s_1^a} \beta
                            \Longleftrightarrow M \models_{s_2^a} \beta
                        \]
                        Thus
                        \begin{align*}
                            M \models_{s_1} \alpha
                            & \Longleftrightarrow M_{s_1^a} \beta \text{for all} a \in W^M \\
                            & \Longleftrightarrow M_{s_2^a} \beta \text{for all} a \in W^M \\
                            & \Longleftrightarrow M \models_{s_2} \alpha \\
                        \end{align*}
                    \item \(\alpha\) is of the form \((\exists x) \beta\).
                        In this case we act like in subcase 2.1., and at the end
                        we get
                        \begin{align*}
                            M \models_{s_1} \alpha
                            & \Longleftrightarrow \text{There exists \(a \in W^M\) such that} M \models_{s_1^a} \beta \\
                            & \Longleftrightarrow \text{There exists \(a \in W^M\) such that} M \models_{s_2^a} \beta \\
                            & \Longleftrightarrow M \models_{s_2} \alpha \\
                        \end{align*}
                \end{itemize}
        \end{itemize}
\end{itemize}

\emph{midterm test on 2.6.22}

\end{document}
