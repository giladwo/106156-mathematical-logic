\documentclass[12pt, a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts}

\title{Lecture 21}
\author{ Gilad Woloch }
\date{\today}

\newcommand{\proof}{\textbf{Proof}: }
\newcommand{\definition}{\textbf{Definition}: }
\newcommand{\theorem}{\textbf{Theorem}: }
\newcommand{\conclusion}{\textbf{Conclusion}: }
\newcommand{\claim}{\textbf{Claim}: }
\newcommand{\example}[1]{\textbf{Example {#1}}: }
\newcommand{\pair}[2]{\left( {#1}, {#2} \right)}

\begin{document}

\maketitle

\section{Untitled}

\theorem
Let \(L\) be a language, let \(M\) be a structure for \(L\).
For all infinite carinals \(\kappa\) that satisfy \(|L| \le \kappa \le |W^M|\),
there exists an elementary sub-structure \(N\) of \(M\) with \(|W^N| = \kappa\).

\begin{proof}
    It is sufficient to find a subset \(W^N \subseteq W^M\) that satisfies
    the following properties:
    \begin{enumerate}
        \item \(|W^N| = \kappa\)
        \item \(W^N\) contains all the constants of \(M\)
        \item \(W^N\) is closed under the functions of \(M\)
        \item \(W^N\) is closed under existential propositions
    \end{enumerate}
    If such subset is found, define structure \(N\) by evaluating individual
    constants using \(M\) and define the functions and relations by narrowing
    the respective functions and relations of \(M\) to the world \(W^N\).

    From the definition of sub-structure and the theorem about the required and
    sufficient for elementariness, this yields an elementary sub-structure
    \(N\) of \(M\) with the required worlds size.

    First, inspect the set
    \[
        C^M = \left\{ c_{i}^{M} \colon c_i \text{\ is an individual variable of language\ } L \right\}
    \]

    There are no more than \(\kappa\) individual constants in the language,
    \[
        |C^M| \le \kappa
    \]
    therefore there exists a set
    \[
        C^M \subseteq W_0 \subseteq W^M
    \]
    such that
    \[
        |W_0| = \kappa
    \]
    % TODO: insert intuition drawing here
    Proceed to construct a chain of subsets of \(W^M\)
    \[
        W_0 \subseteq W_1 \subseteq W_2 \subseteq \dots \subseteq W_i \subseteq \dots
    \]
    where \(W_0\) is the set we've already defined.

    The construction is defined by induction on \(i\), meaning we assume \(W_i\)
    is already defined and construct \(W_{i + 1}\).

    Inspect a formula \(\beta\) in language \(L\) that has \(k + 1\) free
    variables \(x, x_1, x_2, \dots, x_k\) and the
    \(k\)-tuple \(a_1, a_2, \dots, a_k\) of elements of \(W_i\).

    If there exists an element \(a \in W^M\) such that for substitution \(s\)
    that satisfies
    \[
        s(x) = a, s(x_j) = a_j, j = 1, \dots, k
    \]
    it holds that \(M \models_{s} \beta\), then choose such \(a\) and denote
    it by
    \[
        h(\beta, x, a_1, \dots, a_k)
    \]

    Repeat this process for all formulae \(\beta\), for all free variables
    \(x\) and for all \(k\)-tuples \(a_1, \dots, a_k\) of \(W_i\) elements such
    \(a\) exists for.

    Next, define \(W_{i + 1}\):
    % TODO: break condition into multiple lines
    \begin{eqnarray*}
        W_{i + 1} & = W_i \\
                  & \bigcup \left\{ f^M(a_1, \dots, a_k) \colon \text{\(f\) is a \(k\)-ary function symbol in language \(L\), \((a_1, \dots, a_k) \in W_{i}^{k}\)} \right\} \\
                  & \bigcup \left\{ h(\beta, x, a_a, \dots, a_k) \colon \text{\(\beta\) is a formula in language \(L\) with free variables \(x, x_1, \dots, x_k\), \((a_1, \dots, a_k) \in W_{i}^{k}\) such that such \(a\) exists} \right\} \\
    \end{eqnarray*}

    Next, show by induction on \(i\) that \(|W_i| = \kappa\) for all \(i\).

    By construction, it is true for \(i = 0\).

    Assume it is true for \(i\) and prove for \(i + 1\).

    Since \(\kappa\) is an infinite cardinal, the cardinal of the set of all
    \(k\)-tuples of elements of \(W_i\) is also \(\kappa\).

    \(|L| \le \kappa\), hence there are no more than \(\kappa\) function
    symbols in language \(L\).

    Additionally, there are no more than \(\kappa\) formulae in language \(L\).

    Therefore, in the definition of \(W_{i + 1}\) we've added to \(W_i\) no
    more than \(\kappa\) elements, thus \(|W_{i + 1}| = \kappa\) as well.

    Next, define
    \[
        W_{\infty} = \cup_{i = 0}^{\infty} W_{i}
    \]
    and conclude that \(|W_{\infty}| = \kappa\) as a countable union of sets of
    cardinality \(\kappa\).

    To prove that \(W_{\infty}\) satisfies the closure properties 3, 4
    acknowledge that given \((a_1, \dots, a_k) \in W_{\infty}^{k}\), there
    exists an \(i\) such that \((a_1, \dots, a_k) \in W_i^k\), and then the
    construction of \(W_{i + 1}\) guarantees that the desired element is in
    \(W_{i + 1}\), hence it is also in \(W_{\infty}\).

    Finally, take \(W_{\infty}\) as \(W^N\) and all desired properties are
    satisfied.
\end{proof}

\section{L\"owenheim - Skolem theorem}

\theorem
Let \(\Sigma\) be a set of formulae in language \(L\) and assume \(\Sigma\) has
a model with an infinite world.
Then for all infinite cardinals \(\kappa\) that satisfy \(\kappa \ge |L|\)
there exists a model \(N\) of \(\Sigma\) with \(|W^N| = \kappa\).

\begin{proof}
    Let \(M\) be a model of \(\Sigma\) with an infinite world and let
    \(\kappa\) be an infinite cardinal that satisfies \(\kappa \ge |L|\).

    Show that there exists a model \(N\) of \(\Sigma\) with \(|W^N| = \kappa\).

    Split the proof into two cases.

    \underline{The downward part}: \(\kappa \le |W^M|\)

    In this case the previous theorem's conditions are met.

    From the theorem, there exists an elementary sub-structure \(N\) of \(M\)
    with \(|W^N| = \kappa\).

    Show that \(N\) is a model of \(\Sigma\).

    Let \(\alpha\) be a formula in \(\Sigma\).

    Then \(M \models \alpha\), meaning for all substitutions \(s\) for \(M\)
    it holds that \(M \models_{s} \alpha\), and specifically for all
    substitutions \(s\) for \(N\) it holds that \(M \models_{s} \alpha\).
    From elementariness, it follows that for all substitutions \(s\) for \(N\)
    it holds that \(N \models_{s} \alpha\), meaning \(N \models \alpha\).

    Therefore, \(N\) is a model of \(\Sigma\) as required.

    \underline{The upward part}: \(\kappa > |W^M|\)
    First, extend language \(L\) in two ways:
    \begin{itemize}
        \item Add the language a set of new \(\kappa\) individual constants
            \(\{ c_i \colon i \in I \}\), \(|I| = \kappa\)

        \item If the language doesn't have the relation symbol \(=\), add it.
    \end{itemize}

    Let \(L*\) be the extended language.

    Secondly, extend the set of formulae \(\Sigma\) by adding formulae of the
    following form:
    \[
        \neg c_i = c_j, i \not = j, i, j \in I
    \]

    Let \(\Sigma*\) be the extended formulae set.

    Show that \(\Sigma*\) has a model.

    From the compactness theorem, it is sufficient to show that for all finite
    subsets \(\Sigma_0 \subseteq \Sigma*\), \(\Sigma_0\) has a model.

    Let \(\Sigma_0\) be such subset.

    Then it has a finite number of formulae that were added to \(\Sigma\), and
    in these there is a finite number, namely \(k\), of new individual
    constants \(c_i\).

    Take the given model \(M\) of \(\Sigma\) and extend it to be a model of
    language \(L*\) by evaluating the new individual constants in a way that
    the \(k\) values of those that appear in \(\Sigma_0\) are different from
    each other (it is possible because \(W^M\) is infinite).

    This yields a model of \(\Sigma_0\).

    Therefore, from the compactness theorem, there exists a model \(N\) of
    \(\Sigma*\), and the added formulae guaranee \(|W^B| \ge \kappa\).

    Ignore the new individual constants' values and get a structure for
    language \(L\) which is a model of \(\Sigma\).

    If \(|W^N| = \kappa\), we're done.

    If \(|W^N| > \kappa\), use the downward part that's already proven.
\end{proof}

\end{document}
