\documentclass[12pt, a4paper]{article}

\title{Lecture 3}
\author{ Gilad Woloch }
\date{\today}

\begin{document}

\maketitle

\section{Demonstration: using induction on proposition structure}

Let \(A\) be a proposition.

Let \(\bar{A}\) signify the result of replacing any atomic proposition with its
negation.

For instance, if \(A = p \lor ( q \rightarrow \neg r )\) then
\(\bar{A} = \neg p \lor ( \neg q \rightarrow \neg \neg r)\).

Now, we right the definition of \(\bar{A}\) for any proposition \(A\) using
induction on the structure of \(A\):

Induction base:

\begin{itemize}
    \item if \(A = p\) is an atomic proposition then \(\bar{A} = \neg p\).
\end{itemize}

Induction step:
\begin{itemize}
    \item if \(A = \neg B\) then \(\bar{A} = \bar{B}\).
    \item if \(A = B * C\) where \(*\) is one of
        \(\land, \lor, \rightarrow, \equiv\) then \(\bar{A} = \bar{B} * \bar{C}\)
\end{itemize}

Similarly, for any assignment \(M\) we mark the negated assigment \(\bar{M}\),
meaning for each atomic proposition p, \(\bar{M}(p) = T \Leftrightarrow M(p) = F\).


\textbf{Theorem}: For any proposition \(A\) and for any assignment \(M\) it
holds that \(\bar{M} \models \bar{A} \Leftrightarrow M \models A\).

\textbf{Proof}: By induction on proposition \(A\) structure.

Induction base:
\begin{itemize}
    \item if \(A = p\) is an atomic proposition then
        \(\bar{M} \models \bar{A}
        \Leftrightarrow % by definition of \bar{A}
        \bar(M) \models \neg p
        \Leftrightarrow % by definition of truthness
        \bar{M} \not\models p
        \Leftrightarrow % by definition of truthness
        \bar{M}(p) = F
        \Leftrightarrow % by definition of \bar{M}
        M(p) = T
        \Leftrightarrow % by definition of truthness
        M \models p
        \Leftrightarrow % because A = p
        M \models A\)
\end{itemize}

Induction step:
\begin{itemize}
    \item if \(A = \neg B\) then
        \(\bar{M} \models \bar{A}
        \Leftrightarrow % by definition of \bar{A}
        \bar{M} \models \neg \bar{B}
        \Leftrightarrow % by definition of truthness
        \bar{M} \not\models \bar{B}
        \Leftrightarrow % by induction hypothesis
        M \not\models B
        \Leftrightarrow % by definition of truthness
        M \models \neg B
        \Leftrightarrow % because A = \neg B
        M \models A\)

    \item if \(A = B \land C\) then
        \(\bar{M} \models \bar{A}
        \Leftrightarrow % by definition of \bar{A}
        \bar{M} \models \bar{B} \land \bar{C}
        \Leftrightarrow % by definition of truthness
        \bar{M} \models \bar{B} and also \bar{M} \models \bar{C}
        \Leftrightarrow % by induction hypothesis
        M \models B and also M \models C
        \Leftrightarrow % by truthness definition
        M \models B \land C
        \Leftrightarrow % because A = B \land C
        M \models A\)

    The same method proves the theorem for cases \(A = B \lor C, A = B
        \rightarrow C, A = B \equiv C\) and this concludes the induction step
        and the proof.
\end{itemize}


\section{Completeness of a set of connectors}

\textbf{n-ary truth function} is a function
\(f : \{ T, F \}^n \rightarrow \{ T, F \}\).

For example, the following table describes a trinary truth function \(f\):

\begin{center}
    \begin{tabular}{c|c|c|c}
        1 & 2 & 3 & f \\
        \hline
        T & T & T & T \\
        T & T & F & F \\
        T & F & T & T \\
        T & F & F & T \\
        F & T & T & F \\
        F & T & F & F \\
        F & F & T & F \\
        F & F & F & F \\
    \end{tabular}
\end{center}

Given an n-ary truth function \(f\), we say proposition \(A\)
\textbf{represents} f if in \(A\) there are exactly n atomic propositions, say
\(p_1, p_2, ..., p_n\), and for any assignment \(M\) it holds that
\(M \models A \Leftrightarrow f(M(p_1), M(P_2), ..., M(P_n)) = T\).

In other words, the truth table of proposition \(A\) is the truth table of the
truth function \(f\).

For instance, the trinary truth function \(f\) from the example is represented
by the proposition \(A = p_1 \land (p_2 \rightarrow p_3)\).

There are other propositions that represent the same \(f\), but they're all
logically equivalent to each other.

\textbf{Definition}: A set \(K\) of connectors is called \textbf{complete} if
for all natural \(n\) and for all n-ary truth function \(f\) there exists a
proposition \(A\) that's built only with the connectors in \(K\) and represents
\(f\).

A proposition \(A\) is in DNF form (Disjunctive Normal Form) if it's a
disjunction of sub-propositions where each sub-proposition is a conjunction of
atomic propositions or negations thereof, i.e.
\((p \land \neg q \land r) \lor (\neg p \land \neg r)\), \(\neg q \land \neg q\).

\textbf{Theorem}: For all natural \(n\) and for all n-ary truth function \(f\)
there exists a proposition \(A\) in DNF form that represents \(f\).

\textbf{Proof}: Let \(f\) be an \(n\)-ary truth function.
Let \(m\) be the number of rows in the truth table of \(f\) in which \(f\)
returns \(T\).
First, assume \(m >= 1\).
Write proposition \(A\) in DNF form that's buld from \(m\) sub-propositions,
where each of them corresponds with a row in the truth table of \(f\) where
\(f\) returns \(T\), and the atomic propositions \(p_1, p_2, ..., p_n\) appear
as is or negated, according to what appears in that row.
For instance, for the trinary \(f\) from the example, we get
\(A = (p_1 \land p_2 \land p_3)
        \lor ( p_1 \land \neg p_2 \land p_3)
        \lor (p_1 \land \neg p_2 \land \neg p_3)\).
then proposition \(A\) indeed represents the truth function \(f\).
It remains to handle the case \(m = 0\).
In this case, the propostion that represents \(f\) should be a contradiction,
where the atomic propositions \(p_1, p_2, ..., p_n\) appear.
For instance, we can choose
\(p_1 \land \neg p_1 \land p_2 \land p_3 \land ... \land p_n\).

\textbf{Conclusion from the theorem}: the set of all 5 connectors we're working
with is complete.
Furthermore, its subset \(K = \{ \neg, \land, \lor \}\) is already complete.

\textbf{Theorem}: Any of the following connectors subsets is (independently)
complete:
\begin{enumerate}
    \item \(\{\neg, \land\}\)
    \item \(\{\neg, \lor\}\)
    \item \(\{\neg, \rightarrow\}\)
\end{enumerate}



midterm test on 2.6.22

\end{document}
