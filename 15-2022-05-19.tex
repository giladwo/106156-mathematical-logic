\documentclass[12pt, a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts}

\title{Lecture 15}
\author{ Gilad Woloch }
\date{\today}

\newcommand{\proof}{\textbf{Proof}: }
\newcommand{\definition}{\textbf{Definition}: }
\newcommand{\theorem}{\textbf{Theorem}: }
\newcommand{\conclusion}{\textbf{Conclusion}: }
\newcommand{\claim}{\textbf{Claim}: }
\newcommand{\example}[1]{\textbf{Example {#1}}: }
\newcommand{\pair}[2]{\left( {#1}, {#2} \right)}

\begin{document}

\maketitle

\section{Untitled}

\underline{Reminder}:
let \(\alpha\) be a formula, let \(x\) be an individual variable and let \(t\) be a term.

\(\alpha(t)\) is the formula \(\alpha\) yields by a collision-free substitution of \(t\) replacing \(x\).

For a structure \(M\) and a substitution \(s\) we've defined
\[
    \bar s(y) = \begin{cases}
        s(y) & \text{if\ } y \ne x \\
        s^*(t) & \text{if\ } y = x \\
    \end{cases}
\]

We've proved that \((\bar s)^*(r) = s^*(r(t))\).

\underline{Helper claim}:
For all structures \(M\) and for all substitutions \(s\):
\[
    M \models_{\bar s} \alpha \Longleftrightarrow M \models_{s} \alpha(t)
\]

\underline{Proof}:
By induction on the structure of the formula \(\alpha\).

\begin{itemize}
    \item If \(\alpha\) is an atomic formula of the form
        \[
            R(r_1, \dots, r_k)
        \]
        where \(R\) is a \(k\)-ary relation symbol, then
        \(\alpha(t)\) is
        \[
            R(r_1(t), \dots, r_k(t))
        \]
        Therefore
        \begin{align*}
            M \models_{\bar s} \alpha
            & \Longleftrightarrow \left( (\bar s)^*(r_1), \dots, (\bar s)^*(r_k) \right) \in R^M \\
            & \Longleftrightarrow \left( s^*(r_1), \dots, s^*(r_k) \right) \in R^M \\
            & \Longleftrightarrow M \models_{s} \alpha(t) \\
        \end{align*}

    \item If \(\alpha\) is formed from one or multiple simpler formulae then
        one of the following cases applies:
        \begin{itemize}
            \item By a connector
                \begin{itemize}
                    \item \(\alpha\) is of the form \(\neg \beta\)

                        Then \(\alpha(t)\) is \(\neg \beta(t)\) and also
                        the substitution \(\beta(t)\) is collision-free.
                        Thus
                        \[
                            M \models_{\bar s} \alpha
                            \Longleftrightarrow M \not \models_{\bar s} \beta
                            \Longleftrightarrow M \not \models_{s} \beta(t)
                            \Longleftrightarrow M \models_{s} \alpha(t)
                        \]

                    \item \(\alpha\) is of the form \(\beta \land \gamma\)

                        Then \(\alpha(t)\) is \(\beta(t) \land \gamma(t)\) and
                        the substitutions \(\beta(t)\), \(\gamma(t)\) are
                        collision-free.
                        Thus
                        \begin{align*}
                            M \models_{\bar s}
                            & \Longleftrightarrow M \models_{\bar s} \text{and also} M \models_{\bar s} \gamma \\
                            & \Longleftrightarrow M \models_{s} \beta(t) \text{and also} M \models_{s} \gamma(t) \\
                            & \Longleftrightarrow M \models_{s} \alpha(t) \\
                        \end{align*}
                \end{itemize}
                and so on, similarly for the rest of the connectors.

            \item By a quantifier
                \begin{itemize}
                    \item For all
                        \begin{itemize}
                            \item \(\alpha\) is of the form \((\forall x) \beta\)

                                Then \(\alpha(t)\) is \(\alpha\).
                                According to the definition of truthness

                                \(M \models_{\bar s} \alpha\) if and only if
                                for all substitutions \((\bar s)'\) that coalesce
                                with \(\bar s\) except maybe to on \(x\) it holds
                                that \(M \models_{(\bar s)'} \beta\).

                                \(M \models_{s} \alpha\) if and only if
                                for all substitutions \(s'\) that coalesce with
                                \(s\) except maybe to on \(x\) it holds that
                                \(M \models_{s'} \beta\).

                                Since \(\bar s\), \(s\) coalesce except maybe
                                to on \(x\), in both cases it's the same family
                                of substitutions, hence
                                \[
                                    M \models_{\bar s} \alpha \Longleftrightarrow M \models_{s} \alpha(t)
                                \]
                                QED.

                            \item \(\alpha\) is of the form \((\forall y) \beta\)
                                where \(y \ne x\).

                                Then \(\alpha(t)\) is \((\forall y) \beta(t)\)
                                and also \(\beta(t)\) is collision-free.
                                For all elements \(a \in W^M\) we denote:

                                \(s^a\) is the substitution that holds \(s^a(y) = a\)
                                and coalesces with \(s\) otherwise.

                                \((\bar s)^a\) is the substitution that holds \((\bar s)^a(y) = a\)
                                and coalesces with \(\bar s\) otherwise.

                                Now we show that \(\square\)
                                \[
                                    M \models_{(\bar s)^a} \beta
                                    \Longleftrightarrow M \models_{\overline{(s^a)}} \beta
                                \]

                                To do so, we acknowledge that the two substitutions
                                that are used in \(\square\) coalesce on all
                                individual variables that are not \(x\) or \(y\),
                                because they coalesce in these cases with the
                                original \(s\).

                                Furthermore, they coalesce on \(y\) because
                                \[
                                    (\bar s)^a(y) = a = s^a(y) = \overline{(s^a)}(y)
                                \]

                                So the two subtitutions used in \(\square\) can
                                only vary on \(x\).
                                This means that if \(\square\) is not logically
                                true:

                                \begin{enumerate}
                                    \item[(a)] \((\bar s^a)(x) \ne \overline{(s^a)}(x)\)
                                    \item[(b)] \(x\) freely appears in \(\beta\).
                                \end{enumerate}

                                Let's see what can be deduced from (a).
                                \[
                                    (\bar s)^a(x) = \bar s(x) = s^*(t)
                                \]
                                \[
                                    \overline{(s^a)}(x) = (s^a)^*(t)
                                \]
                                and since \(s\), \(s^a\) can only vary on \(y\),
                                we deduce from (a) that
                                \begin{enumerate}
                                    \item[(c)] \(y\) appears in \(t\)
                                \end{enumerate}
                                but (b) + (c) mean that the free substitution
                                of \(t\) replacing \(x\) in formula \(\alpha\)
                                collides, which contradicts the assumption.
                        \end{itemize}

                    \item Exists

                        The discussion is analogous to 1.b. (For all), with
                        every "for all" replaced with "there exists".
                \end{itemize}
        \end{itemize}
\end{itemize}


\emph{midterm test on 2.6.22}

\end{document}
