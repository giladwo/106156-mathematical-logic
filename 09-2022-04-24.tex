\documentclass[12pt, a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts}

\title{Lecture 9}
\author{ Gilad Woloch }
\date{\today}

\newcommand{\proof}{\textbf{Proof}: }
\newcommand{\definition}{\textbf{Definition}: }
\newcommand{\theorem}{\textbf{Theorem}: }
\newcommand{\conclusion}{\textbf{Conclusion}: }
\newcommand{\claim}{\textbf{Claim}: }

\begin{document}

\maketitle

\section{The compactness theorem}

\theorem (The compactness theorem)
Let \(\Sigma\) be a set of propositions.
Then \(\Sigma\) has a model if and only if for all finite
\(\Sigma_{0} \subset \Sigma\) have models.

\proof

\underline{First direction}:

If \(\Sigma\) has a model \(M\), then \(M\) is a model of all finite
\(\Sigma_{0} \subseteq \Sigma\), so all such \(\Sigma_{0}\) have a model.

\underline{Second direction}:

Assume all finite \(\Sigma_{0} \subseteq \Sigma\) have a model.

Negatively assume \(\Sigma\) doesn't have a model.

According to the previous theorem we've proved, \(\Sigma\) is contradictory,

meaning there exists a proposition \(A\) such that \(\Sigma \vdash A\) and also
\(\Sigma \vdash \neg A\).

Inspect both proofs: \(A\) from \(\Sigma\) and \(\neg A\) from \(\Sigma\).

Since a proof is a finite sequence of propositions, each of them contains only
a finite number of propositions from \(\Sigma\).

Let \(\Sigma_{0} \subseteq \Sigma\) be the set of all the propositions which
appear in at least one of the two proofs.

Then \(\Sigma_{0}\) is finite and it follows that both \(\Sigma_{0} \vdash A\)
and \(\Sigma_{0} \vdash \neg A\).

Again, according to the previous theorem we've proved, it means that
\(\Sigma_{0}\) doesn't have a model, contradictorily to our assumption.


\section{An application to the map coloring problem}

The map coloring problem:

Can any planar map, where the countries are connected, be colored, each country
in one of 4 given colors, in a way such that all pairs of countries that share
a common border are not colored in the same color?

\definition A graph \(G \left(V, E\right)\) is given by its \underline{vertices}
\(V\) and is \underline{edges} \(E\), where: \(V\) is a some non-empty set (may
be infinite) and \(E\) is a set of unordered pairs of vertices.

A graph is called \underline{finite} if \(V\) the vertices set is finite.

A \underline{coloring} of \(G\) of \(k\) colors is a function
\[
    c \colon V \rightarrow \left\{ 1, 2, \dots, k \right\}
\]
such that
\[
    \left\{ u, v \right\} \in E \Rightarrow c \left( u \right) \not = c \left( v \right)
\]

For instance, to color the following graph
\begin{verbatim}
          2
          *
         /|\
        / | \
     1 *  |  * 1
        \ | /
         \|/
          *
          3
\end{verbatim}
we only need 3 colors as shown.

We say that \(G\) is \underline{\(k\)-colorable} if there exists a coloring of
it with \(k\) colors.

We say that \(G\) is \underline{planar} if it can be painted on a plane without
any edges crossing one another.

In these terms, one can (equivalently) rephrase the coloring problem of maps
like so:
Is any finite planar graph \(4\)-colorable?

\theorem \underline{The four colors theorem} (Appel-Haken, 1976)
Yes.

\definition Let \(G = \left( V, E \right)\) be a graph and let
\(U \subseteq V\) be the \underline{inducted sub-graph} of \(G\) on \(U\),
denoted as \(G \left[ U \right]\), is the graph that its vertices set is \(U\),
its edges set is the set of all pairs \( \left\{ x, y \right\} \) such that
\(x, y \in U\) and \(\left\{x, y\right\} \in E\).

\theorem (Erd\"os-DeBruijn, 1961)

Let \(G = \left( V, E \right)\) be a graph and let \(k\) be a natural number.

Assume that any finite inducted sub-graph \(G \left[ U \right]\) of \(G\) is
\(k\)-colorable.

Then \(G\) itself is \(k\)-colorable.

\conclusion Any planar graph is \(4\)-colorable (even infinite).

\proof (Erd\"os-DeBruijn theorem)

Given a graph \(G = \left( V, E \right)\) and the natural number \(k\), inspect
the propositional calculus where the set of the atomic propositions is
\[
    % should be curly AP (Atomic Propositions)
    AP = \left\{ p_{v, i} \colon v \in V, i \in \left\{ 1, \dots, k \right\} \right\}
\]
where \(p_{v, i}\) means that the vertice \(v\) is colored with the color \(i\).

For all subsets \(U \subseteq V\) define the following proposition sets:

\[
    \Sigma_{1} \left( U \right) = \left\{ p_{v, 1} \lor p_{v, 2} \lor \cdots \lor p_{v, k} \colon v \in U \right\}
\]
This proposition means: vertice \(v\) is colored with at least one color.

\[
    \Sigma_{2} \left( U \right) = \left\{ \neg \left( p_{v, i} \land p_{v, j} \right) \colon v \in U, i, j \in \left\{ 1, \dots, k \right\}, i \not = j \right\}
\]
These propositions mean: vertice \(v\) is colored with at most one color.

\[
    \Sigma_{3} \left( U \right) = \left\{ \neg \left( p_{u, i} \land p_{v, i} \right) \colon \left\{ u, v \right\} \in E, u, v \in U, i \in \left\{ 1, \dots, k \right\} \right\}
\]
These propositions mean: the coloring is valid.

Finally, define
\[
    \Sigma \left( U \right) = \Sigma_{1} \left( U \right) \cup \Sigma_{2} \left( U \right) \cup \Sigma_{3} \left( U \right) \cup \Sigma \left( U \right)
\]

\claim Given a sub-set \(U \subseteq V\) it holds that
\(\Sigma \left( U \right)\) has a model if and only if \(G \left[ U \right]\)
is \(k\)-colorable.

\proof

\underline{First direction}:

Assume that \(\Sigma \left( U \right)\) has a model \(M\).

Then define a coloring \(c\) of \(G \left[ U \right]\) with \(k\) colors like
so:
\[
    c \left( v \right) = i \Leftrightarrow M \models p_{v, i}
\]
Given the truthness of the propositions from
\(\Sigma_{1} \left( U \right) \cup \Sigma_{2} \left( U \right)\), this function
is well-defined, and given the propositions from \(\Sigma_{3} \left( U \right)\)
it is valid.

\underline{Second direction}:

Assume \(G \left[ U \right]\) has a coloring \(c\) of \(k\) colors.

Then define an assignment \(M\) the following way:

For \(v \in U\) and \(i \in \left\{ 1, \dots, k \right\}\):
\[
    M \left( p_{v, i} \right) = T \Leftrightarrow c \left( v \right) = i
\]
and for \(v \not \in U\) define the assignment \(M\) arbitrarily.

Given that \(c\) is a function from (on?) \(U\), the assignment \(M\) satisfies
all the propositions of
\(\Sigma_{1} \left( U \right) \cup \Sigma \left( U \right)\), and given that
it's valid, the assignment satisfies the propositions of
\(\Sigma_{3} \left( U \right)\).

\par Now, back to the theorem's proof.

We need to show that the graph \(G = G \left[ U \right]\) is \(k\)-colorable.

Given the claim (for \(U = V\)) we need to show that \(\Sigma \left( V \right)\)
has a model.

Let \(\Sigma_{0}\) be such a finite sub-set.

Let \(U_{0}\) be the set of all vertices that apear as indices of atomic
propositions that appear in the propositions of \(\Sigma_{0}\).

Then \(U_{0}\) is finite, and it holds that
\[
    \Sigma_{0} \subseteq \Sigma \left( U_{0} \right)
\]

It is sufficient to show that \(\Sigma \left( U_{0} \right)\) has a model.

According to the claim it is equivalent to \(G \left[ U_{0} \right]\) being
\(k\)-colorable, which is true according to the assumption of the theorem.


\emph{midterm test on 2.6.22}

\end{document}
