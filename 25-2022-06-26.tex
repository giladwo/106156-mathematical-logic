\documentclass[12pt, a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts}

\title{Lecture 25}
\author{ Gilad Woloch }
\date{\today}

\newcommand{\proof}{\textbf{Proof}: }
\newcommand{\definition}{\textbf{Definition}: }
\newcommand{\theorem}{\textbf{Theorem}: }
\newcommand{\conclusion}{\textbf{Conclusion}: }
\newcommand{\claim}{\textbf{Claim}: }
\newcommand{\example}[2]{\textbf{Example {#1}}: {#2}\\}
\newcommand{\pair}[2]{\left( {#1}, {#2} \right)}

\begin{document}

\maketitle

\section{Number theory and G\"odel's Incompleteness Theorem}

The language of number theory contains
\begin{itemize}
    \item An individual constant \(c\)
    \item An unary function symbol \(S\)
    \item Binary function symbols \(f\), \(g\), \(h\)
    \item Binary relation symbols \(R\), \(=\)
\end{itemize}

\underline{The standard structure} for the language of number theory is the
following structure \(N\):

\begin{itemize}
    \item \(W^N = \left\{ 0, 1, 2, \dots \right\}\) the non-negative integers.

    \item \(c^N = 0\) the zero element.

    \item\(S^N(a) = a + 1\) the successor function.

    \item\(f^N(a, b) = a + b\) the addition function.

    \item\(g^N(a, b) = a * b\) the multiplication function.

    \item\(h^N(a, b) = a^b\) the exponentiation function.

    \item\(R^N = \left\{ (a, b) \colon a < b \right\}\) the order relation.
\end{itemize}


In this language one can formulate theorems about the natural numbers, for
example:
\[
    \neg (\exists x) (\exists y) (\exists z) (\exists u)
    (f(h(x, u), h(y, u)) = h(z, u)
     \land \neg x = c
     \land y = c
     \land R(S(S(c)), u))
\]

This sentence expresses Fermat's Hypothesis, which became Wiles Theorem.

For all natural numbers \(k\), we'll use the following shorthand notation
\[
    S^k(c)
\]
for
\[
    \underbrace{S(S(\cdots(S}_{k \text{\ times}}(c))\cdots))
\]
This is a term in the language and understood as the natural number \(k\).


\subsection{G\"odel's Incompleteness Theorem}

Let \(\Sigma\) be a theory in the language of number theory such that
\begin{enumerate}
    \item \(N\) is a model of \(\Sigma\)
    \item \(\Sigma\) is recursive
\end{enumerate}
Then \(\Sigma\) is incomplete.


\subsubsection{Conclusions from the theorem}

\begin{itemize}
    \item There is no reasonable axiomatization for number theory.

        For all axiomatic systems \(\Sigma\), there exists a sentence \(\phi\)
        in the language of number theory that is logically true in the
        standard`structure \(N\) but is not provable from \(\Sigma\).

    \item Inspect the theory
        \[
            \Sigma = \{ \phi : N \models \phi, \phi \text{\ is a sentence} \}
        \]

        It is obviously complete and satisfies the first condition of the
        theorem.

        Therefore, from the theorem follows that \(\Sigma\) is not recursive.

    \item From G\"odel's strong completeness theorem, if a sentence \(\phi\)
        is logically true in all models of \(\Sigma\) than it is provable from
        \(\Sigma\).

        Alas, from G\"odel's incompleteness theorem, there exists a sentence
        \(\phi\) that is logically true in the standard structure \(N\) but is
        not provable from \(\Sigma\).

        Hence, for all such axiomatic systems \(\Sigma\), there exist
        \underline{non-standard models} of number theory (in addition to the
        standard model)>
\end{itemize}


\subsection{The concept of the proof}

"The liar paradox":
A person states on themselves "I'm lying".

Similarly, inspect:
A sentence that formulates on itself "I'm not provable from \(\Sigma\)".

If such sentence is found, we can show that neither it nor its negation are
provable from \(\Sigma\), thus \(\Sigma\) is incomplete.

The problem: sentences in the language of number theory are about arithmetics
and not about syntactics.


\subsection{The arithmeticization of syntactics}

First, let any symbol that may appear in a formula in the language of number
theory relate to an (aribtrary) natural number.

This is demonstrated in the following table:
\begin{center}
    \begin{tabular}{c c}
        Symbol & Numeric value \\
        \hline \hline
        \(c\) & 1 \\
        \(S\) & 2 \\
        \(f\) & 3 \\
        \(g\) & 4 \\
        \(h\) & 5 \\
        \(R\) & 6 \\
        \(=\) & 7 \\
        \(\neg\)  & 8 \\
        \(\rightarrow\) & 9 \\
        \(\forall\) & 10 \\
        \((\) & 11 \\
        \()\) & 12 \\
        \(,\) & 13 \\
        \(x_1\) & 14 \\
        \(x_2\) & 15 \\
        \(x_3\) & 16 \\
        \(\vdots\) & \(\vdots\) \\
    \end{tabular}
\end{center}

Now, for all formulae \(\alpha\) in the language of number theory, relate a
natural number that encodes it, which is
\[
    2^{a_1} \cdot 3^{a_2} \cdot 5^{a_3} \cdot 7^{a_4} \cdot \cdots
\]
where \(a_i\) is thenumerical value of the \(i\)-th symbol in formula
\(\alpha\).

For instance, for the following formula \(\alpha\)
\[
    ( \forall x_1 ) x_1 = x_2
\]
the number that encodes it is
\[
    2^{11} \cdot 3^{10} \cdot 5^{14} \cdot 7^{12} \cdot 11^{14} \cdot 13^{7} \cdot 17^{15}
\]

In this form, all formulae \(\alpha\) is encoded as a natural number that is
denoted as \(\#\alpha\) and is called the \underline{G\"odel number} of the
formula \(\alpha\).

Each natural number is a G\"odel numb of at most a single formula.

Now, for all finite series \(\alpha_1\), \(\alpha_2\), \(\dots\), \(\alpha_n\)
of formulae in the language of number theory, relate a natural number that
encodes it, which is
\[
    2^{\#\alpha_1} \cdot 3^{\#\alpha_2} \cdot 5^{\#\alpha_3} \cdot \cdots
\]

In this form, all finite series \(\alpha_1\), \(\alpha_2\), \(\dots\), \(\alpha_n\),
of formulae in the language of number theory are encoded by a natural number
that is denoted as \(\#(\alpha_1, \alpha_2, \dots, \alpha_n)\) which is called
the \underline{G\"odel number} of the series \(\alpha_1\), \(\alpha_2\), \(\dots\), \(alpha_n\).

Likewise, each natural numbers is a G\"odel number of at most a single series
of formulae.

\underline{Lemma}:
Let \(\Sigma\) be a theory as required by the theorem.
Then there exits a formula \(\alpha\) in the language of number theory that has
3 free variables \(x_1\), \(x_2\), \(x_3\) that has the following property:

For a 3-tuple \(k\), \(l\), \(m\) of natural numbers denote by
\[
    \alpha(S^k(c), S^l(c), S^m(c))
\]
the sentence that is given from formula \(\alpha\) by the free substitution of

\(S^k(c)\) replacing \(x_1\)

\(S^l(c)\) replacing \(x_2\)

\(S^m(c)\) replacing \(x_3\)

then for all natural numbers \(k\), \(l\), \(m\) it holds that:

\[
    N \models \alpha(S^k(c), S^l(c), S^m(c)) \\
\]

if and only if

\(k\) is the G\"odel number of formula \(\beta\) with a single free variable,
\(m\) is the G\"odel number of a proof from \(\Sigma\) of the sentence given by
formula \(\beta\) after the free substitution of \(S^l(c)\) replacing the free
variable.



\end{document}
